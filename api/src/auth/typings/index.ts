export interface Me {
  name: string;
  email: string;
  permissions: string[];
  profiles: string[];
}
