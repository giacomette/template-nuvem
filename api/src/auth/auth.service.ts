import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { User } from 'infra/database/models/User';
import { Permission } from 'infra/database/models/Permission';
import { Me } from './typings';
import { UsersQueries } from 'users/users.queries';

@Injectable()
export class AuthService {
  constructor(
    private usersQueries: UsersQueries,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, pass: string): Promise<any> {
    const user = await this.usersQueries.findOne(email);

    if (user && bcrypt.compareSync(pass, user.password)) {
      const { password, ...result } = user;
      return result;
    }

    return null;
  }

  async login(user: User) {
    const payload = { id: user.id, sub: user.id };

    return {
      accessToken: this.jwtService.sign(payload),
    };
  }

  async me(userId: string): Promise<Me> {
    const user = await User.findOne({
      where: { id: userId },
      relations: ['profiles', 'person'],
    });

    const result: Me = {
      email: user.email,
      name: user.person.name,
      permissions: [],
      profiles: user.profiles.map((item) => item.name),
    };

    const profilesIds = user.profiles.map((item) => item.id);

    if (profilesIds.length) {
      const permissions = await Permission.createQueryBuilder('p')
        .select('p.tag', 'tag')
        .innerJoin('p.profilePermissions', 'profilePermissions')
        .where('profilePermissions.profileId IN (:...ids)', {
          ids: profilesIds,
        })
        .groupBy('p.tag')
        .getRawMany();

      result.permissions = permissions.map((item) => item.tag);
    }

    return result;
  }
}
