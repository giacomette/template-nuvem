import { Module } from '@nestjs/common';
import { PermissionsController } from './permissions.controller';
import { PermissionsQueries } from './permissions.queries';

@Module({
  providers: [PermissionsQueries],
  exports: [PermissionsQueries],
  controllers: [PermissionsController],
})
export class PermissionsModule {}
