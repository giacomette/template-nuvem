import { Controller, Get, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'auth/guards/jwt-auth.guard';

import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { PermissionsQueries } from './permissions.queries';

@Controller({
  path: 'permissions',
  version: '1',
})
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiTags('Permissões')
export class PermissionsController {
  constructor(private profilesQueries: PermissionsQueries) {}

  @Get('/')
  @ApiOperation({
    summary: 'Listar permissões',
  })
  @ApiResponse({
    status: 200,
    schema: {
      example: [
        {
          id: 'e9af31dd-43e8-4fa1-97e0-44a200cd276b',
          name: 'Wesley',
          parentId: 'e9af31dd-43e8-4fa1-97e0-44a200cd276b',
        },
      ],
    },
  })
  list() {
    return this.profilesQueries.findAll();
  }
}
