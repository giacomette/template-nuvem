export interface PermissionItem {
  id: string;
  name: string;
  parentId: string;
}
