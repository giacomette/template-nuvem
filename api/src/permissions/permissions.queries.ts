import { Injectable } from '@nestjs/common';
import { Permission } from 'infra/database/models/Permission';
import { PermissionItem } from './typings';

@Injectable()
export class PermissionsQueries {
  async findAll(): Promise<PermissionItem[]> {
    const queryBuilder = Permission.createQueryBuilder('p');

    const result = await queryBuilder.getMany();

    const items: PermissionItem[] = result.map((item) => ({
      id: item.id,
      name: item.name,
      parentId: item.parentId,
    }));

    return items;
  }
}
