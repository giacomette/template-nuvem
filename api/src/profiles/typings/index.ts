export interface ProfilePaginate {
  id: string;
  name: string;
}

export interface ProfileDetails {
  id: string;
  name: string;
  permissions: string[];
}
