import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class ProfileCreateRequest {
  @IsNotEmpty({ message: 'O nome é obrigatório' })
  @ApiProperty({
    required: true,
    example: 'John doe',
    description: 'Nome do perfil',
  })
  readonly name: string;

  @IsNotEmpty({ message: 'Informe pelo menos uma permissão' })
  @ApiProperty({
    required: true,
    example: ['e9af31dd-43e8-4fa1-97e0-44a200cd276b'],
    description: 'permissoes do perfil',
  })
  readonly permissions: string[];
}

export class ProfileUpdateRequest extends ProfileCreateRequest {}
