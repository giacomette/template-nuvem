import { Module } from '@nestjs/common';
import { ProfilesController } from './profiles.controller';
import { ProfilesQueries } from './profiles.queries';
import { ProfilesService } from './profiles.service';

@Module({
  providers: [ProfilesService, ProfilesQueries],
  exports: [ProfilesService, ProfilesQueries],
  controllers: [ProfilesController],
})
export class ProfilesModule {}
