import { Injectable } from '@nestjs/common';
import { Profile } from 'infra/database/models/Profile';
import { ProfilePermission } from 'infra/database/models/ProfilePermission';
import { makePaginateOption, PaginateRequest } from 'infra/helpers/paginate';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { ProfileDetails, ProfilePaginate } from './typings';

@Injectable()
export class ProfilesQueries {
  async findAll(
    options: PaginateRequest,
    tenantId: string,
  ): Promise<Pagination<ProfilePaginate>> {
    const queryBuilder = Profile.createQueryBuilder('p');

    queryBuilder.where('p.tenantId = :tenantId', {
      tenantId,
    });

    const result = await paginate<Profile>(
      queryBuilder,
      makePaginateOption(options),
    );

    const items: ProfilePaginate[] = result.items.map((item) => ({
      id: item.id,
      name: item.name,
    }));

    return { ...result, items };
  }

  async findById(id: string): Promise<ProfileDetails> {
    const user = await Profile.createQueryBuilder('p')
      .where('p.id = :id', {
        id,
      })
      .getOne();

    const profilePermissions = await ProfilePermission.find({
      where: {
        profileId: id,
      },
    });

    const result: ProfileDetails = {
      id: user.id,
      name: user.name,
      permissions: profilePermissions.map((p) => p.permissionId),
    };

    return result;
  }
}
