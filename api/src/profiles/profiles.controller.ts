import {
  Controller,
  Get,
  Post,
  UseGuards,
  Body,
  Put,
  Param,
  Query,
  Delete,
  Req,
} from '@nestjs/common';
import { JwtAuthGuard } from 'auth/guards/jwt-auth.guard';
import { ProfileCreateRequest, ProfileUpdateRequest } from './requests';
import { ProfilesService } from './profiles.service';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ProfilesQueries } from './profiles.queries';
import { PaginateRequest } from 'infra/helpers/paginate';
import { CheckPermission } from 'infra/guards/permission.decorator';
import { PERMISSIONS } from 'infra/resource/enums/permissions';

@Controller({
  path: 'profiles',
  version: '1',
})
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiTags('Perfis')
export class ProfilesController {
  constructor(
    private profilesService: ProfilesService,
    private profilesQueries: ProfilesQueries,
  ) {}

  @Get('/')
  @ApiOperation({
    summary: 'Listar perfis',
  })
  @ApiResponse({
    status: 200,
    schema: {
      example: {
        meta: {
          totalItems: 1,
          itemCount: 1,
          itemsPerPage: 20,
          totalPages: 1,
          currentPage: 1,
        },
        items: [
          {
            id: 'e9af31dd-43e8-4fa1-97e0-44a200cd276b',
            name: 'Wesley',
          },
        ],
      },
    },
  })
  @CheckPermission(PERMISSIONS.Perfil.Visualizar)
  list(
    @Req() req,
    @Query()
    query: PaginateRequest = {
      limit: 20,
      page: 1,
    },
  ) {
    return this.profilesQueries.findAll(query, req.headers.tenant);
  }

  @Get(':id')
  @ApiOperation({
    summary: 'Obter perfil',
  })
  @ApiResponse({
    status: 200,
    schema: {
      example: {
        id: 'e9af31dd-43e8-4fa1-97e0-44a200cd276b',
        name: 'Wesley',
        permissions: ['e9af31dd-43e8-4fa1-97e0-44a200cd276b'],
      },
    },
  })
  @CheckPermission(PERMISSIONS.Perfil.Visualizar)
  findId(@Param('id') id: string) {
    return this.profilesQueries.findById(id);
  }

  @Post('/')
  @ApiOperation({
    summary: 'Criar perfil',
  })
  @ApiResponse({
    status: 200,
    schema: {
      example: {
        data: 'e9af31dd-43e8-4fa1-97e0-44a200cd276b',
      },
    },
    description: `Perfil criado`,
  })
  @CheckPermission(PERMISSIONS.Perfil.Criar)
  public async create(@Req() req, @Body() body: ProfileCreateRequest) {
    const result = await this.profilesService.create(body, req.headers.tenant);

    return { data: result };
  }

  @Put(':id')
  @ApiOperation({
    summary: 'Editar perfil',
  })
  @ApiResponse({
    status: 200,
    schema: {
      example: {
        data: 'e9af31dd-43e8-4fa1-97e0-44a200cd276b',
      },
    },
    description: `Perfil editado`,
  })
  @CheckPermission(PERMISSIONS.Perfil.Editar)
  public async update(
    @Param('id') id: string,
    @Body() body: ProfileUpdateRequest,
  ) {
    const result = await this.profilesService.update(id, body);

    return { data: result };
  }

  @Delete(':id')
  @ApiOperation({
    summary: 'Excluir perfil',
  })
  @ApiResponse({
    status: 200,
    description: `Perfil excluido`,
  })
  @CheckPermission(PERMISSIONS.Perfil.Excluir)
  public async delete(@Param('id') id: string) {
    const result = await this.profilesService.delete(id);

    return { data: result };
  }
}
