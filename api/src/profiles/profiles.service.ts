import { Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';
import { ProfileCreateRequest, ProfileUpdateRequest } from './requests';
import { Profile } from 'infra/database/models/Profile';
import { ProfilePermission } from 'infra/database/models/ProfilePermission';

@Injectable()
export class ProfilesService {
  constructor(private dataSource: DataSource) {}

  async create(data: ProfileCreateRequest, tenantId: string): Promise<string> {
    return this.dataSource.transaction(async (manager) => {
      const profile = new Profile({
        name: data.name,
        tenantId,
      });

      await manager.save(profile);

      const profilePermissions = data.permissions.map(
        (item) =>
          new ProfilePermission({
            profileId: profile.id,
            permissionId: item,
          }),
      );

      await manager.save(profilePermissions);

      return profile.id;
    });
  }

  async update(id: string, data: ProfileUpdateRequest): Promise<string> {
    const profile = await Profile.findOne({
      where: { id },
    });

    profile.fill({
      name: data.name,
    });

    return this.dataSource.transaction(async (manager) => {
      await manager.save(profile);

      await manager.update(
        ProfilePermission,
        {
          profileId: profile.id,
        },
        {
          deletedAt: new Date().toISOString(),
        },
      );

      const profilePermissions = data.permissions.map(
        (item) =>
          new ProfilePermission({
            profileId: profile.id,
            permissionId: item,
          }),
      );

      await manager.save(profilePermissions);

      return profile.id;
    });
  }

  async delete(id: string): Promise<void> {
    const profile = await Profile.findOne({
      where: { id },
    });

    await this.dataSource.transaction((manager) => manager.softRemove(profile));
  }
}
