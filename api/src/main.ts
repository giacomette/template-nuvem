import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import appConfig from './infra/config';
import { json, urlencoded } from 'express';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import helmet from 'helmet';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.use(helmet());
  app.enableVersioning();

  app.use(json({ limit: '50mb' }));
  app.use(urlencoded({ extended: true, limit: '150mb' }));

  const config = new DocumentBuilder()
    .setTitle('Nuvem apis')
    .setDescription('Documentação das apis')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);

  app.useGlobalPipes(
    new ValidationPipe({
      validationError: {
        target: true,
        value: true,
      },
    }),
  );

  await app.listen(appConfig().port ?? 2000);
}

bootstrap();
