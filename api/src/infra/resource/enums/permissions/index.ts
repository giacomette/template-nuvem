export const PERMISSIONS = {
  Usuario: {
    Visualizar: 'usuario.visualizar',
    Criar: 'usuario.criar',
    Editar: 'usuario.editar',
    Excluir: 'usuario.excluir',
  },
  Perfil: {
    Visualizar: 'perfil.visualizar',
    Criar: 'perfil.criar',
    Editar: 'perfil.editar',
    Excluir: 'perfil.excluir',
  },
};
