export default () => ({
  database: {
    host: process.env.DATABASE_HOST,
    port: Number(process.env.DATABASE_PORT),
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
  },
  port: parseInt(process.env.PORT, 10) || 2000,
  isProd: process.env.NODE_ENV === 'production',
  sentry: {
    dsn: process.env.SENTRY_DSN,
    tracesSampleRate: 1.0,
    debug: process.env.NODE_ENV !== 'production',
    environment: process.env.NODE_ENV !== 'production' ? 'dev' : 'production',
  },
  emailSender: '',
  sendGridApi: process.env.SENDGRID_API_KEY,
});
