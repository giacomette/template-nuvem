import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { registerAs } from '@nestjs/config';
import appConfig from './';

import * as dotenv from 'dotenv';
import { DataSource } from 'typeorm';
import { User } from 'infra/database/models/User';
import { UserProfile } from 'infra/database/models/UserProfile';
import { Profile } from 'infra/database/models/Profile';
import { ProfilePermission } from 'infra/database/models/ProfilePermission';
import { Permission } from 'infra/database/models/Permission';
import { Tenant } from 'infra/database/models/Tenant';
import { TenantUser } from 'infra/database/models/TenantUser';
import { Animal } from 'infra/database/models/Animal';
import { AnimalCategory } from 'infra/database/models/AnimalCategory';
import { AnimalType } from 'infra/database/models/AnimalType';
import { Address } from 'infra/database/models/Address';
import { AddressType } from 'infra/database/models/AddressType';
import { City } from 'infra/database/models/City';
import { Company } from 'infra/database/models/Company';
import { Contact } from 'infra/database/models/Contact';
import { ContactType } from 'infra/database/models/ContactType';
import { Customer } from 'infra/database/models/Customer';
import { Cylinder } from 'infra/database/models/Cylinder';
import { Movement } from 'infra/database/models/Movement';
import { MovementType } from 'infra/database/models/MovementType';
import { Partner } from 'infra/database/models/Partner';
import { PersonType } from 'infra/database/models/PersonType';
import { PhysicalPerson } from 'infra/database/models/PhysicalPerson';
import { Race } from 'infra/database/models/Race';
import { State } from 'infra/database/models/State';
import { Person } from 'infra/database/models/Person';

dotenv.config();

export const optionsDatabase: TypeOrmModuleOptions = {
  type: 'mysql',
  host: appConfig().database.host,
  port: appConfig().database.port,
  username: appConfig().database.username,
  password: appConfig().database.password,
  database: appConfig().database.database,
  entities: [
    Person,
    Partner,
    PersonType,
    PhysicalPerson,
    Race,
    City,
    State,
    Company,
    Contact,
    ContactType,
    Cylinder,
    MovementType,
    Movement,
    Customer,
    Address,
    AddressType,
    Animal,
    AnimalType,
    AnimalCategory,
    User,
    UserProfile,
    Profile,
    ProfilePermission,
    Permission,
    Tenant,
    TenantUser,
  ],
  migrations: [__dirname + '/../database/migrations/*.ts'],
  synchronize: false,
  logging: true,
  logger: 'debug',
};

const databaseSource = new DataSource({
  type: optionsDatabase.type,
  host: optionsDatabase.host,
  port: optionsDatabase.port,
  username: optionsDatabase.username,
  password: optionsDatabase.password,
  database: optionsDatabase.database,
  entities: optionsDatabase.entities,
  migrations: optionsDatabase.migrations,
  synchronize: optionsDatabase.synchronize,
  logging: optionsDatabase.logging,
  logger: optionsDatabase.logger,
});

export default databaseSource;

export const db = registerAs(
  'database',
  (): TypeOrmModuleOptions => optionsDatabase,
);
