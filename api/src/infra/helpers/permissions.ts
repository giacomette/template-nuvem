import { ForbiddenException } from '@nestjs/common';
import { Permission } from 'infra/database/models/Permission';

export type Origem = 'app' | 'backoffice';

export async function checkPermission(
  data: {
    permission: string | string[];
    tenantId: string;
    userId: string;
  },
  throwsException = false,
): Promise<boolean> {
  if (!data.tenantId) {
    throw new ForbiddenException('Tenant invalido');
  }

  const permissionsString =
    typeof data.permission === 'string'
      ? []
      : data.permission.map((permission) => `'${permission}'`);

  const sql = `SELECT p.* FROM permissions AS p
    INNER JOIN profilePermissions AS pp ON pp.permissionId = p.id
    INNER JOIN userProfiles AS up ON up.profileId = pp.profileId
    INNER JOIN profiles AS pe ON pe.id = pp.profileId
    WHERE pe.tenantId = '${data.tenantId}' AND ${
    typeof data.permission === 'string'
      ? `p.tag = '${data.permission}'`
      : `p.tag in (${permissionsString.join(',')})`
  }  AND up.userId = '${
    data.userId
  }' AND up.deletedAt IS NULL AND pp.deletedAt IS NULL AND pe.deletedAt IS NULL LIMIT 1`;

  const [hasPermission] = await Permission.query(sql);

  // if (!hasPermission) {
  //   const [admin] = await manager.query(
  //     `SELECT TOP 1 tu.* FROM tenantUsers AS tu WHERE tu.deletedAt IS NULL AND tu.tenantId = '${data.tenantId}' AND tu.userId = '${data.userId}'`,
  //   );

  //   hasPermission = !!admin?.admin;
  // }

  if (!hasPermission && throwsException) {
    throw new ForbiddenException('Sem permissão para acessar esse recurso');
  }

  return !!hasPermission;
}
