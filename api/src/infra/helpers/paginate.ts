import { ApiProperty } from '@nestjs/swagger';
import { IPaginationOptions } from 'nestjs-typeorm-paginate';

export function makePaginateOption(options: IPaginationOptions) {
  const paginateOptions: IPaginationOptions = {
    ...options,
  };

  if (!paginateOptions.page || paginateOptions.page <= 0) {
    paginateOptions.page = 1;
  }

  if (!paginateOptions.limit || paginateOptions.limit <= 0) {
    paginateOptions.limit = 20;
  }

  return paginateOptions;
}

export class PaginateRequest implements IPaginationOptions {
  @ApiProperty({
    example: 1,
    description: 'Página da consulta',
  })
  readonly page: number = 1;

  @ApiProperty({
    example: 20,
    description: 'Limite da consulta',
  })
  readonly limit: number = 20;

  @ApiProperty({
    required: false,
    description: 'Busca pelo texto',
  })
  readonly search?: string;
}
