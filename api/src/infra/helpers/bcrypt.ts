import * as bcrypt from 'bcrypt';

export function crypt(value: string) {
  const saltRounds = 10;

  const salt = bcrypt.genSaltSync(saltRounds);
  const hash = bcrypt.hashSync(value, salt);

  return hash;
}

export function compareCrypt(value: string, valueCrypted: string) {
  return bcrypt.compareSync(value, valueCrypted);
}
