import { Entity, Column } from 'typeorm';
import { BaseEntity } from './BaseEntity';

@Entity('animalTypes')
export class AnimalType extends BaseEntity<AnimalType> {
  @Column()
  name: string;
}
