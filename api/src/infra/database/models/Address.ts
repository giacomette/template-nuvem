import { Entity, Column, ManyToOne } from 'typeorm';
import { BaseEntity } from './BaseEntity';
import { City } from './City';
import { Person } from './Person';
import { State } from './State';

@Entity('addresses')
export class Address extends BaseEntity<Address> {
  @Column({
    nullable: true,
  })
  zipcode: string;

  @Column({
    length: 255,
    nullable: true,
  })
  street: string;

  @Column({
    length: 255,
    nullable: true,
  })
  number: string;

  @Column('geometry', {
    nullable: true,
  })
  latLong: any;

  @Column({
    length: 255,
    nullable: true,
  })
  googlePlace: string;

  @Column({
    length: 255,
    nullable: true,
  })
  complement: string;

  @Column({
    length: 255,
    nullable: true,
  })
  district: string;

  @Column({
    length: 255,
    nullable: true,
  })
  observation: string;

  @Column('uuid', {
    nullable: true,
  })
  stateId!: string;

  @Column('uuid', {
    nullable: true,
  })
  cityId: string;

  @Column('uuid', {
    nullable: true,
  })
  personId: string;

  @ManyToOne(() => State)
  state!: State;

  @ManyToOne(() => City)
  city!: City;

  @ManyToOne(() => Person)
  person!: Person;
}
