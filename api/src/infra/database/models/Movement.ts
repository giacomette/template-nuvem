import { Entity, Column, ManyToOne } from 'typeorm';
import { Animal } from './Animal';
import { BaseEntity } from './BaseEntity';
import { Cylinder } from './Cylinder';
import { MovementType } from './MovementType';
import { Person } from './Person';

@Entity('movements')
export class Movement extends BaseEntity<Movement> {
  @Column()
  operation: 1 | -1 | number;

  @Column()
  date: string;

  @Column()
  observation: string;

  @Column()
  reference: string;

  @Column()
  quantity: number;

  @Column('decimal', {
    precision: 10,
    scale: 3,
  })
  unityValue: number;

  @Column('uuid')
  movementTypeId: string;

  @Column('uuid')
  animalId: string;

  @Column('uuid')
  personId: string;

  @Column('uuid')
  cylinderId: string;

  @ManyToOne(() => Animal)
  animal: Animal;

  @ManyToOne(() => Cylinder)
  cylinder: Cylinder;

  @ManyToOne(() => Person)
  person: Person;

  @ManyToOne(() => MovementType)
  MovementType: MovementType;
}
