import { Entity, Column } from 'typeorm';
import { BaseEntity } from './BaseEntity';

@Entity('movementTypes')
export class MovementType extends BaseEntity<MovementType> {
  @Column()
  name: string;
}
