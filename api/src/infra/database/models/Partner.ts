import { Entity, Column, ManyToOne } from 'typeorm';
import { BaseEntity } from './BaseEntity';
import { Person } from './Person';

@Entity('partners')
export class Partner extends BaseEntity<Partner> {
  @Column('uuid')
  personId: string;

  @Column()
  abs: string;

  @ManyToOne(() => Person)
  person: Person;
}
