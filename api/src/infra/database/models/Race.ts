import { Entity, Column } from 'typeorm';
import { BaseEntity } from './BaseEntity';

@Entity('races')
export class Race extends BaseEntity<Race> {
  @Column()
  name: string;
}
