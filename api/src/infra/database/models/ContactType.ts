import { Entity, Column } from 'typeorm';
import { BaseEntity } from './BaseEntity';

@Entity('contactTypes')
export class ContactType extends BaseEntity<ContactType> {
  @Column()
  name: string;
}
