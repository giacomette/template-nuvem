import {
  Entity,
  Column,
  ManyToMany,
  JoinTable,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { BaseEntity } from './BaseEntity';
import { Profile } from './Profile';
import { ProfilePermission } from './ProfilePermission';

@Entity('permissions')
export class Permission extends BaseEntity<Permission> {
  @Column()
  name: string;

  @Column()
  tag: string;

  @Column()
  parentId: string;

  @ManyToOne(() => Permission)
  parent: Permission;

  @OneToMany(() => ProfilePermission, (type) => type.permission)
  profilePermissions: ProfilePermission;

  @ManyToMany(() => Profile, (type) => type.permissions)
  @JoinTable({
    name: 'profilePermissions',
    joinColumn: {
      name: 'permissionId',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'profileId',
      referencedColumnName: 'id',
    },
  })
  profiles: Profile[];
}
