import { Entity, Column, ManyToOne } from 'typeorm';
import { BaseEntity } from './BaseEntity';
import { Person } from './Person';

@Entity('physicalPeople')
export class PhysicalPerson extends BaseEntity<PhysicalPerson> {
  @Column()
  birthday: string;

  @Column('uuid')
  personId: string;

  @ManyToOne(() => Person)
  person: Person;

  @Column()
  gender: string;
}
