import { Entity, Column, OneToOne, OneToMany, ManyToOne } from 'typeorm';
import { Address } from './Address';
import { BaseEntity } from './BaseEntity';
import { Company } from './Company';
import { Contact } from './Contact';
import { PersonType } from './PersonType';
import { PhysicalPerson } from './PhysicalPerson';

@Entity('people')
export class Person extends BaseEntity<Person> {
  @Column()
  name: string;

  @Column({
    nullable: true,
  })
  imageUrl: string;

  @Column({
    length: 255,
    nullable: true,
  })
  document: string;

  @Column('uuid', {
    nullable: true,
  })
  personTypeId: string;

  @OneToOne(() => Address, (address) => address.person)
  address!: Address;

  @OneToOne(() => PhysicalPerson, (physicalPerson) => physicalPerson.person)
  physicalPerson!: PhysicalPerson;

  @OneToOne(() => Company, (company) => company.person)
  company!: Company;

  @ManyToOne(() => PersonType)
  personType!: PersonType;

  @OneToMany(() => Address, (address) => address.person)
  addresses: Address[];

  @OneToMany(() => Contact, (contact) => contact.person)
  contacts: Contact[];
}
