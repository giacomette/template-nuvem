import {
  Entity,
  Column,
  OneToMany,
  ManyToMany,
  JoinTable,
  ManyToOne,
} from 'typeorm';
import { BaseEntity } from './BaseEntity';
import { Permission } from './Permission';
import { Tenant } from './Tenant';
import { User } from './User';
import { UserProfile } from './UserProfile';

@Entity('profiles')
export class Profile extends BaseEntity<Profile> {
  @Column()
  name: string;

  @Column()
  tenantId: string;

  @ManyToOne(() => Tenant)
  tenant: Tenant;

  @OneToMany(() => UserProfile, (up) => up.profile)
  profileUsers: UserProfile[];

  @ManyToMany(() => Permission, (type) => type.profiles)
  @JoinTable({
    name: 'profilePermissions',
    joinColumn: {
      name: 'profileId',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'permissionId',
      referencedColumnName: 'id',
    },
  })
  permissions: Permission[];

  @ManyToMany(() => User, (type) => type.profiles)
  @JoinTable({
    name: 'userProfiles',
    joinColumn: {
      name: 'profileId',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'userId',
      referencedColumnName: 'id',
    },
  })
  users: User[];
}
