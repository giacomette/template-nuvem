import { Entity, Column } from 'typeorm';
import { BaseEntity } from './BaseEntity';

@Entity('addressTypes')
export class AddressType extends BaseEntity<AddressType> {
  @Column()
  name: string;
}
