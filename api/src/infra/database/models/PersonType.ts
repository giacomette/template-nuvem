import { Entity, Column, OneToMany } from 'typeorm';
import { BaseEntity } from './BaseEntity';
import { Person } from './Person';

@Entity('personTypes')
export class PersonType extends BaseEntity {
  @Column({
    length: 255,
  })
  name: string;

  @OneToMany(() => Person, (person) => person.personType)
  person!: Person;
}
