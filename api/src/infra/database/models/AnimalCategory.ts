import { Entity, Column } from 'typeorm';
import { BaseEntity } from './BaseEntity';

@Entity('animalCategories')
export class AnimalCategory extends BaseEntity<AnimalCategory> {
  @Column()
  name: string;
}
