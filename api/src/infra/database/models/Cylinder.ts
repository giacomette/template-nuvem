import { Entity, Column } from 'typeorm';
import { BaseEntity } from './BaseEntity';

@Entity('cylinders')
export class Cylinder extends BaseEntity<Cylinder> {
  @Column()
  name: string;
}
