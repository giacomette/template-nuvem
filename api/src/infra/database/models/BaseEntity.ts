import * as moment from 'moment';
import {
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  DeleteDateColumn,
  BeforeInsert,
  BaseEntity as BaseEntityTypeOrm,
  UpdateDateColumn,
} from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

export class BaseEntity<T = any> extends BaseEntityTypeOrm {
  constructor(data?: Partial<T>) {
    super();

    Object.assign(this, data);
  }

  fill(data: Partial<T>) {
    Object.keys(data).forEach((key) => {
      if (data[key] !== undefined) {
        this[key] = data[key];
      }
    });
  }

  @Column('uuid', {
    nullable: true,
  })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn()
  createdAt!: string;

  @UpdateDateColumn()
  updatedAt!: string;

  @DeleteDateColumn()
  deletedAt?: string;

  @BeforeInsert()
  async beforeInsert() {
    for (const prop in this) {
      if (this[prop as any] === '') {
        this[prop] = null;
      }
    }
    if (!this.id) {
      this.id = uuidv4();
    }

    if (!this.createdAt) {
      this.createdAt = moment().format();
    }

    if (!this.updatedAt) {
      this.updatedAt = moment().format();
    }
  }
}
