import { Entity, Column, ManyToOne } from 'typeorm';
import { AnimalCategory } from './AnimalCategory';
import { AnimalType } from './AnimalType';
import { BaseEntity } from './BaseEntity';
import { Race } from './Race';

@Entity('animals')
export class Animal extends BaseEntity<Animal> {
  @Column('uuid')
  animalTypeId: string;

  @Column('uuid')
  animalCategoryId: string;

  @Column('uuid')
  raceId: string;

  @Column()
  name: string;

  @Column()
  abs: string;

  @Column()
  rack: string;

  @ManyToOne(() => AnimalType)
  animalType!: AnimalType;

  @ManyToOne(() => AnimalCategory)
  animalCategory!: AnimalCategory;

  @ManyToOne(() => Race)
  race!: Race;
}
