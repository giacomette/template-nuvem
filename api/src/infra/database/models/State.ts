import { Entity, Column } from 'typeorm';
import { BaseEntity } from './BaseEntity';

@Entity('states')
export class State extends BaseEntity {
  @Column()
  name!: string;

  @Column()
  uf!: string;
}
