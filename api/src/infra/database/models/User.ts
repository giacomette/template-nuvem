import {
  Entity,
  Column,
  OneToMany,
  ManyToMany,
  JoinTable,
  ManyToOne,
} from 'typeorm';
import { BaseEntity } from './BaseEntity';
import { Person } from './Person';
import { Profile } from './Profile';
import { UserProfile } from './UserProfile';

@Entity('users')
export class User extends BaseEntity<User> {
  @Column()
  personId: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column('boolean')
  active: boolean;

  @ManyToOne(() => Person)
  person!: Person;

  @OneToMany(() => UserProfile, (up) => up.profile)
  userProfiles: UserProfile[];

  @ManyToMany(() => Profile, (type) => type.users)
  @JoinTable({
    name: 'userProfiles',
    joinColumn: {
      name: 'userId',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'profileId',
      referencedColumnName: 'id',
    },
  })
  profiles: Profile[];
}
