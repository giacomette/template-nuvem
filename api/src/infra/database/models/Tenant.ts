import { Entity, Column, ManyToOne } from 'typeorm';
import { BaseEntity } from './BaseEntity';

@Entity('tenants')
export class Tenant extends BaseEntity<Tenant> {
  @Column()
  name: string;

  @Column()
  parentId: string;

  @ManyToOne(() => Tenant)
  parent: Tenant;
}
