import { Entity, Column, ManyToOne } from 'typeorm';
import { BaseEntity } from './BaseEntity';
import { Permission } from './Permission';
import { Profile } from './Profile';

@Entity('profilePermissions')
export class ProfilePermission extends BaseEntity<ProfilePermission> {
  @Column()
  permissionId: string;

  @Column()
  profileId: string;

  @ManyToOne(() => Profile)
  profile: Profile;

  @ManyToOne(() => Permission)
  permission: Permission;
}
