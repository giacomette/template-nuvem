import { Entity, Column, ManyToOne } from 'typeorm';
import { Person } from './Person';
import { BaseEntity } from './BaseEntity';

@Entity('companies')
export class Company extends BaseEntity<Company> {
  @Column()
  fantasyName: string;

  @Column('uuid')
  personId: string;

  @ManyToOne(() => Person)
  person!: Person;
}
