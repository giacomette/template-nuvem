import { Entity, Column, ManyToOne } from 'typeorm';
import { BaseEntity } from './BaseEntity';
import { Tenant } from './Tenant';
import { User } from './User';

@Entity('tenantUsers')
export class TenantUser extends BaseEntity<TenantUser> {
  @Column()
  tenantId: string;

  @Column()
  userId: string;

  @ManyToOne(() => Tenant)
  tenant: Tenant;

  @ManyToOne(() => User)
  user: User;
}
