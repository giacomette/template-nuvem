import { Entity, Column, ManyToOne } from 'typeorm';
import { BaseEntity } from './BaseEntity';
import { Person } from './Person';

@Entity('customers')
export class Customer extends BaseEntity<Customer> {
  @Column()
  website: string;

  @Column()
  observation: string;

  @Column('uuid')
  personId: string;

  @ManyToOne(() => Person)
  person: Person;
}
