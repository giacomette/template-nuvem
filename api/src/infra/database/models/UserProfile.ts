import { Entity, Column, ManyToOne } from 'typeorm';
import { BaseEntity } from './BaseEntity';
import { Profile } from './Profile';
import { User } from './User';

@Entity('userProfiles')
export class UserProfile extends BaseEntity<UserProfile> {
  @Column()
  userId: string;

  @Column()
  profileId: string;

  @ManyToOne(() => Profile)
  profile: Profile;

  @ManyToOne(() => User)
  user: User;
}
