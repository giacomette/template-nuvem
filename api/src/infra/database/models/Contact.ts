import { Entity, Column, ManyToOne } from 'typeorm';
import { BaseEntity } from './BaseEntity';
import { Person } from './Person';
@Entity('contacts')
export class Contact extends BaseEntity<Contact> {
  @Column()
  content: string;

  @Column()
  type: string;

  @Column('uuid')
  personId: string;

  @ManyToOne(() => Person)
  person!: Person;
}
