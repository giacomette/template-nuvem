import { Entity, Column, OneToOne } from 'typeorm';
import { State } from './State';
import { BaseEntity } from './BaseEntity';

@Entity('cities')
export class City extends BaseEntity<City> {
  @Column()
  name!: string;

  @Column()
  ibge!: string;

  @Column('uuid')
  stateId!: string;

  @OneToOne(() => State)
  state!: State;
}
