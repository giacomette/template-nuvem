import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class MovementTypes1612673917918 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'movementTypes',
        columns: [
          {
            name: 'id',
            type: 'varchar(36)',
            isPrimary: true,
          },
          {
            name: 'name',
            type: 'varchar(250)',
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'deletedAt',
            type: 'timestamp',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    await queryRunner.query(
      `INSERT INTO movementTypes (id, name) VALUES ('53cebf19-c89e-4c1c-af94-98f0d3e95340', 'Entrada')`,
    );

    await queryRunner.query(
      `INSERT INTO movementTypes (id, name) VALUES ('53cebf19-c89e-4c1c-af94-98f0d3e95341', 'Saida')`,
    );

    await queryRunner.query(
      `INSERT INTO movementTypes (id, name) VALUES ('53cebf19-c89e-4c1c-af94-98f0d3e95342', 'Nota Fiscal')`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('movementTypes');
  }
}
