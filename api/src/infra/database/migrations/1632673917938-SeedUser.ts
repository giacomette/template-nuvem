import { MigrationInterface, QueryRunner } from 'typeorm';

import * as uuid from 'uuid';
import * as bcrypt from 'bcrypt';

export class SeedUser1612673917918 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const profileId = uuid.v4();
    const tenantId = uuid.v4();
    const userId = uuid.v4();

    const saltRounds = 10;

    const password = '123456';

    const salt = bcrypt.genSaltSync(saltRounds);
    const hash = bcrypt.hashSync(password, salt);

    await queryRunner.query(
      `INSERT INTO tenants (id, name) VALUES ('${tenantId}', 'Tucunaré')`,
    );

    await queryRunner.query(
      `INSERT INTO profiles (id, name, tenantId) VALUES ('${profileId}', 'Admin', '${tenantId}')`,
    );

    await queryRunner.query(
      `INSERT INTO users (id, name, email, password) VALUES ('${userId}', 'Wesley', 'wesley@nuvem.net', '${hash}')`,
    );

    const permissaoUsuario = '53cebf19-c89e-4c1c-af94-98f0d3e95342';
    const permissaoPerfil = '53cebf19-c89e-4c1c-af94-98f0d3e95341';

    await queryRunner.query(
      `INSERT INTO permissions (id, name, tag) VALUES ('${permissaoUsuario}', 'Usuários', 'usuario')`,
    );

    await queryRunner.query(
      `INSERT INTO permissions (id, name, tag, parentId) VALUES ('${uuid.v4()}', 'Visualizar', 'usuario.visualizar', '${permissaoUsuario}')`,
    );

    await queryRunner.query(
      `INSERT INTO permissions (id, name, tag, parentId) VALUES ('${uuid.v4()}', 'Criar', 'usuario.criar', '${permissaoUsuario}')`,
    );

    await queryRunner.query(
      `INSERT INTO permissions (id, name, tag, parentId) VALUES ('${uuid.v4()}', 'Editar', 'usuario.editar', '${permissaoUsuario}')`,
    );

    await queryRunner.query(
      `INSERT INTO permissions (id, name, tag, parentId) VALUES ('${uuid.v4()}', 'Excluir', 'usuario.excluir', '${permissaoUsuario}')`,
    );

    // PERFIL
    await queryRunner.query(
      `INSERT INTO permissions (id, name, tag) VALUES ('${permissaoPerfil}', 'Perfis', 'perfil')`,
    );

    await queryRunner.query(
      `INSERT INTO permissions (id, name, tag, parentId) VALUES ('${uuid.v4()}', 'Visualizar', 'perfil.visualizar', '${permissaoPerfil}')`,
    );

    await queryRunner.query(
      `INSERT INTO permissions (id, name, tag, parentId) VALUES ('${uuid.v4()}', 'Criar', 'perfil.criar', '${permissaoPerfil}')`,
    );

    await queryRunner.query(
      `INSERT INTO permissions (id, name, tag, parentId) VALUES ('${uuid.v4()}', 'Editar', 'perfil.editar', '${permissaoPerfil}')`,
    );

    await queryRunner.query(
      `INSERT INTO permissions (id, name, tag, parentId) VALUES ('${uuid.v4()}', 'Excluir', 'perfil.excluir', '${permissaoPerfil}')`,
    );
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  public async down(queryRunner: QueryRunner): Promise<void> {}
}
