import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class TenantUsers1612673917918 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'tenantUsers',
        columns: [
          {
            name: 'id',
            type: 'varchar(36)',
            isPrimary: true,
          },
          {
            name: 'tenantId',
            type: 'varchar(36)',
          },
          {
            name: 'userId',
            type: 'varchar(36)',
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'deletedAt',
            type: 'timestamp',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'tenantUsers',
      new TableForeignKey({
        columnNames: ['userId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'users',
        onDelete: 'NO ACTION',
      }),
    );

    await queryRunner.createForeignKey(
      'tenantUsers',
      new TableForeignKey({
        columnNames: ['tenantId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'tenants',
        onDelete: 'NO ACTION',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const table = await queryRunner.getTable('tenantUsers');

    const foreignKeyTenant = table.foreignKeys.find((fk) =>
      fk.columnNames.includes('tenantId'),
    );

    const foreignKeyUsuario = table.foreignKeys.find((fk) =>
      fk.columnNames.includes('userId'),
    );

    await queryRunner.dropForeignKey('tenantUsers', foreignKeyUsuario);
    await queryRunner.dropForeignKey('tenantUsers', foreignKeyTenant);

    await queryRunner.dropTable('tenantUsers');
  }
}
