import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AddressTypes1612636744220 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'addressTypes',
        columns: [
          {
            name: 'id',
            type: 'varchar(36)',
            isPrimary: true,
          },
          {
            name: 'name',
            type: 'varchar(50)',
          },
          {
            name: 'stateId',
            type: 'varchar(36)',
            isNullable: true,
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'deletedAt',
            type: 'timestamp',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    await queryRunner.query(
      `INSERT INTO addressTypes (id, name) VALUES ('53cebf19-c89e-4c1c-af94-98f0d3e95340', 'Faturamento')`,
    );

    await queryRunner.query(
      `INSERT INTO addressTypes (id, name) VALUES ('53cebf19-c89e-4c1c-af94-98f0d3e95341', 'Entrega')`,
    );

    await queryRunner.query(
      `INSERT INTO addressTypes (id, name) VALUES ('53cebf19-c89e-4c1c-af94-98f0d3e95342', 'Normal')`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('addressTypes');
  }
}
