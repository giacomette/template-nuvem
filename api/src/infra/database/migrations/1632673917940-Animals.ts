import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class Animals1612673917918 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'animals',
        columns: [
          {
            name: 'id',
            type: 'varchar(36)',
            isPrimary: true,
          },
          {
            name: 'animalTypeId',
            type: 'varchar(36)',
          },
          {
            name: 'animalCategoryId',
            type: 'varchar(36)',
          },
          {
            name: 'raceId',
            type: 'varchar(36)',
          },
          {
            name: 'name',
            type: 'varchar(250)',
          },
          {
            name: 'abs',
            type: 'varchar(50)',
          },
          {
            name: 'rack',
            type: 'varchar(50)',
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'deletedAt',
            type: 'timestamp',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'animals',
      new TableForeignKey({
        columnNames: ['raceId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'races',
        onDelete: 'NO ACTION',
      }),
    );

    await queryRunner.createForeignKey(
      'animals',
      new TableForeignKey({
        columnNames: ['animalTypeId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'animalTypes',
        onDelete: 'NO ACTION',
      }),
    );

    await queryRunner.createForeignKey(
      'animals',
      new TableForeignKey({
        columnNames: ['animalCategoryId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'animalCategories',
        onDelete: 'NO ACTION',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const table = await queryRunner.getTable('animals');

    await Promise.all(
      table.foreignKeys.map((item) =>
        queryRunner.dropForeignKey('animals', item),
      ),
    );

    await queryRunner.dropTable('animals');
  }
}
