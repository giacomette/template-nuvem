import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class Movements1612673917918 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'movements',
        columns: [
          {
            name: 'id',
            type: 'varchar(36)',
            isPrimary: true,
          },
          {
            name: 'operation',
            type: 'int',
          },
          {
            name: 'movementTypeId',
            type: 'varchar(36)',
          },
          {
            name: 'animalId',
            type: 'varchar(36)',
          },
          {
            name: 'cylinderId',
            type: 'varchar(36)',
          },
          {
            name: 'personId',
            type: 'varchar(36)',
          },
          {
            name: 'date',
            type: 'datetime',
          },
          {
            name: 'observation',
            type: 'text',
            isNullable: true,
          },
          {
            name: 'quantity',
            type: 'int',
          },
          {
            name: 'unityValue',
            type: 'decimal(10, 3)',
          },
          {
            name: 'reference',
            type: 'varchar(100)',
            isNullable: true,
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'deletedAt',
            type: 'timestamp',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'movements',
      new TableForeignKey({
        columnNames: ['personId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'people',
        onDelete: 'NO ACTION',
      }),
    );

    await queryRunner.createForeignKey(
      'movements',
      new TableForeignKey({
        columnNames: ['cylinderId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'cylinders',
        onDelete: 'NO ACTION',
      }),
    );

    await queryRunner.createForeignKey(
      'movements',
      new TableForeignKey({
        columnNames: ['animalId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'animals',
        onDelete: 'NO ACTION',
      }),
    );

    await queryRunner.createForeignKey(
      'movements',
      new TableForeignKey({
        columnNames: ['movementTypeId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'movementTypes',
        onDelete: 'NO ACTION',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('movements');
  }
}
