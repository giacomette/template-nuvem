import { MigrationInterface, QueryRunner } from 'typeorm';

import * as uuid from 'uuid';

export class SeedProfile1632673917939 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const [user] = await queryRunner.query(`SELECT * FROM users`);
    const [profile] = await queryRunner.query(`SELECT * FROM profiles`);
    const permissions = await queryRunner.query(`SELECT * FROM permissions`);

    for (const p of permissions) {
      await queryRunner.query(
        `INSERT INTO profilePermissions (id, profileId, permissionId) VALUES ('${uuid.v4()}', '${
          profile.id
        }', '${p.id}')`,
      );
    }

    await queryRunner.query(
      `INSERT INTO userProfiles (id, profileId, userId) VALUES ('${uuid.v4()}', '${
        profile.id
      }', '${user.id}')`,
    );
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  public async down(queryRunner: QueryRunner): Promise<void> {}
}
