import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class Races1612673917918 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'races',
        columns: [
          {
            name: 'id',
            type: 'varchar(36)',
            isPrimary: true,
          },
          {
            name: 'name',
            type: 'varchar(250)',
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'deletedAt',
            type: 'timestamp',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    await queryRunner.query(
      `INSERT INTO races (id, name) VALUES ('53cebf19-c89e-4c1c-af94-98f0d3e95342', 'Nelore')`,
    );

    await queryRunner.query(
      `INSERT INTO races (id, name) VALUES ('53cebf19-c89e-4c1c-af94-98f0d3e95341', 'Red Angus')`,
    );

    await queryRunner.query(
      `INSERT INTO races (id, name) VALUES ('53cebf19-c89e-4c1c-af94-98f0d3e95343', 'Holandes')`,
    );

    await queryRunner.query(
      `INSERT INTO races (id, name) VALUES ('53cebf19-c89e-4c1c-af94-98f0d3e95344', 'Girolando')`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('races');
  }
}
