import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class Address1612636744220 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'addresses',
        columns: [
          {
            name: 'id',
            type: 'varchar(36)',
            isPrimary: true,
          },
          {
            name: 'personId',
            type: 'varchar(36)',
          },
          {
            name: 'stateId',
            type: 'varchar(36)',
          },
          {
            name: 'cityId',
            type: 'varchar(36)',
          },
          {
            name: 'zipcode',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'googlePlace',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'street',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'complement',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'number',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'latLong',
            type: 'geometry',
            isNullable: true,
          },
          {
            name: 'district',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'observation',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'addressTypeId',
            type: 'varchar(36)',
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'deletedAt',
            type: 'timestamp',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    // await queryRunner.createForeignKey(
    //   'addresses',
    //   new TableForeignKey({
    //     columnNames: ['personId'],
    //     referencedColumnNames: ['id'],
    //     referencedTableName: 'people',
    //     onDelete: 'NO ACTION',
    //   }),
    // );

    await queryRunner.createForeignKey(
      'addresses',
      new TableForeignKey({
        columnNames: ['cityId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'cities',
        onDelete: 'NO ACTION',
      }),
    );

    await queryRunner.createForeignKey(
      'addresses',
      new TableForeignKey({
        columnNames: ['stateId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'states',
        onDelete: 'NO ACTION',
      }),
    );

    await queryRunner.createForeignKey(
      'addresses',
      new TableForeignKey({
        columnNames: ['addressTypeId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'addressTypes',
        onDelete: 'NO ACTION',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const table = await queryRunner.getTable('addresses');

    const foreignKeyPessoa = table.foreignKeys.find((fk) =>
      fk.columnNames.includes('personId'),
    );

    const foreignKeyCidade = table.foreignKeys.find((fk) =>
      fk.columnNames.includes('cityId'),
    );

    const foreignKeyEstado = table.foreignKeys.find((fk) =>
      fk.columnNames.includes('stateId'),
    );

    await queryRunner.dropForeignKey('addresses', foreignKeyPessoa);
    await queryRunner.dropForeignKey('addresses', foreignKeyCidade);
    await queryRunner.dropForeignKey('addresses', foreignKeyEstado);
    await queryRunner.dropTable('addresses');
  }
}
