import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class ContactTypes1612636744220 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'contactTypes',
        columns: [
          {
            name: 'id',
            type: 'varchar(36)',
            isPrimary: true,
          },
          {
            name: 'name',
            type: 'varchar',
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'deletedAt',
            type: 'timestamp',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    await queryRunner.query(
      `INSERT INTO contactTypes (id, name) VALUES ('53cebf19-c89e-4c1c-af94-98f0d3e95342', 'Celular')`,
    );

    await queryRunner.query(
      `INSERT INTO contactTypes (id, name) VALUES ('53cebf19-c89e-4c1c-af94-98f0d3e95341', 'Telefone')`,
    );

    await queryRunner.query(
      `INSERT INTO contactTypes (id, name) VALUES ('53cebf19-c89e-4c1c-af94-98f0d3e95343', 'Email')`,
    );

    await queryRunner.query(
      `INSERT INTO contactTypes (id, name) VALUES ('53cebf19-c89e-4c1c-af94-98f0d3e95344', 'Whatsapp')`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('contactTypes');
  }
}
