import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class Companies1612636744220 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'companies',
        columns: [
          {
            name: 'id',
            type: 'varchar(36)',
            isPrimary: true,
          },
          {
            name: 'personId',
            type: 'varchar(36)',
          },
          {
            name: 'fantasyName',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'deletedAt',
            type: 'timestamp',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    // await queryRunner.createForeignKey(
    //   'companies',
    //   new TableForeignKey({
    //     columnNames: ['personId'],
    //     referencedColumnNames: ['id'],
    //     referencedTableName: 'people',
    //     onDelete: 'NO ACTION',
    //   }),
    // );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const table = await queryRunner.getTable('companies');

    const foreignKeyPessoa = table.foreignKeys.find((fk) =>
      fk.columnNames.includes('personId'),
    );

    await queryRunner.dropForeignKey('companies', foreignKeyPessoa);
    await queryRunner.dropTable('companies');
  }
}
