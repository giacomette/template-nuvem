import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class Contacts1612636744220 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'contacts',
        columns: [
          {
            name: 'id',
            type: 'varchar(36)',
            isPrimary: true,
          },
          {
            name: 'personId',
            type: 'varchar(36)',
          },
          {
            name: 'content',
            type: 'varchar',
          },
          {
            name: 'typeId',
            type: 'varchar(36)',
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'deletedAt',
            type: 'timestamp',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    // await queryRunner.createForeignKey(
    //   'contacts',
    //   new TableForeignKey({
    //     columnNames: ['personId'],
    //     referencedColumnNames: ['id'],
    //     referencedTableName: 'people',
    //     onDelete: 'NO ACTION',
    //   }),
    // );

    await queryRunner.createForeignKey(
      'contacts',
      new TableForeignKey({
        columnNames: ['typeId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'contactTypes',
        onDelete: 'NO ACTION',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const table = await queryRunner.getTable('contacts');

    const foreignKeyPessoa = table.foreignKeys.find((fk) =>
      fk.columnNames.includes('personId'),
    );

    const foreignKeyContactType = table.foreignKeys.find((fk) =>
      fk.columnNames.includes('contactTypes'),
    );

    await queryRunner.dropForeignKey('contacts', foreignKeyContactType);
    await queryRunner.dropForeignKey('contacts', foreignKeyPessoa);
    await queryRunner.dropTable('contacts');
  }
}
