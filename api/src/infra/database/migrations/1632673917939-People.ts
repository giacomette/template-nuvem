import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class People1612673917918 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'people',
        columns: [
          {
            name: 'id',
            type: 'varchar(36)',
            isPrimary: true,
          },
          {
            name: 'personTypeId',
            type: 'varchar(36)',
          },
          {
            name: 'name',
            type: 'varchar(36)',
          },
          {
            name: 'document',
            type: 'varchar(36)',
            isNullable: true,
          },
          {
            name: 'imageUrl',
            type: 'text',
            isNullable: true,
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'deletedAt',
            type: 'timestamp',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'people',
      new TableForeignKey({
        columnNames: ['personTypeId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'personTypes',
        onDelete: 'NO ACTION',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const table = await queryRunner.getTable('people');

    await Promise.all(
      table.foreignKeys.map((item) =>
        queryRunner.dropForeignKey('people', item),
      ),
    );

    await queryRunner.dropTable('people');
  }
}
