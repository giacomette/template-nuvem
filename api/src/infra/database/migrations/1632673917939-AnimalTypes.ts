import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AnimalTypes1612673917918 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'animalTypes',
        columns: [
          {
            name: 'id',
            type: 'varchar(36)',
            isPrimary: true,
          },
          {
            name: 'name',
            type: 'varchar(250)',
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'NOW()',
          },
          {
            name: 'deletedAt',
            type: 'timestamp',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    await queryRunner.query(
      `INSERT INTO animalTypes (id, name) VALUES ('53cebf19-c89e-4c1c-af94-98f0d3e95342', 'Touro')`,
    );

    await queryRunner.query(
      `INSERT INTO animalTypes (id, name) VALUES ('53cebf19-c89e-4c1c-af94-98f0d3e95341', 'Vaca')`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('animalTypes');
  }
}
