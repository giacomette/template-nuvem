import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { Request } from 'express';
import { PermissionGuard } from './permission.guard';

type fn = (req: Request) => any;

export function CheckPermission(permissaoOr: string | string[] | fn) {
  return applyDecorators(
    SetMetadata('permission', permissaoOr),
    UseGuards(PermissionGuard),
  );
}
