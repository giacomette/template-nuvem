import {
  Injectable,
  CanActivate,
  ExecutionContext,
  ForbiddenException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { checkPermission } from 'infra/helpers/permissions';

type fn = (req: Request) => any;

@Injectable()
export class PermissionGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate = async (context: ExecutionContext) => {
    const permission = this.reflector.get<string | string[] | fn>(
      'permission',
      context.getHandler(),
    );

    if (!permission) {
      throw new ForbiddenException('Sem permissão para acessar esse recurso');
    }

    const request = context.switchToHttp().getRequest<Request>();

    let p: string | string[];

    if (typeof permission === 'function') {
      p = permission(request);
    } else {
      p = permission;
    }

    const tenant = (request.headers as any).tenant;
    const userId = (request.user as any).userId;

    const hasPermission = await checkPermission({
      permission: p,
      tenantId: tenant,
      userId,
    });

    if (!hasPermission) {
      throw new ForbiddenException('Sem permissão para acessar esse recurso');
    }

    return true;
  };
}
