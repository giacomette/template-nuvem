export interface UserPaginate {
  id: string;
  name: string;
  email: string;
  active: boolean;
  profiles: string[];
}

export interface UserDetails {
  id: string;
  name: string;
  email: string;
  active: boolean;
  profiles: string[];
}
