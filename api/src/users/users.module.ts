import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersQueries } from './users.queries';
import { UsersService } from './users.service';

@Module({
  providers: [UsersService, UsersQueries],
  exports: [UsersService, UsersQueries],
  controllers: [UsersController],
})
export class UsersModule {}
