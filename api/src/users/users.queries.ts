import { Injectable } from '@nestjs/common';
import { User } from 'infra/database/models/User';
import { UserProfile } from 'infra/database/models/UserProfile';
import { makePaginateOption, PaginateRequest } from 'infra/helpers/paginate';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { UserDetails, UserPaginate } from './typings';

@Injectable()
export class UsersQueries {
  async findAll(
    options: PaginateRequest,
    tenantId: string,
  ): Promise<Pagination<UserPaginate>> {
    const queryBuilder = User.createQueryBuilder('u')
      .innerJoinAndSelect('u.profiles', 'profiles')
      .innerJoinAndSelect('u.person', 'person');

    queryBuilder.where('profiles.tenantId = :tenantId', {
      tenantId,
    });

    const result = await paginate<User>(
      queryBuilder,
      makePaginateOption(options),
    );

    const items: UserPaginate[] = result.items.map((item) => ({
      active: item.active,
      email: item.email,
      id: item.id,
      name: item.person.name,
      profiles: item.profiles.map((p) => p.name),
    }));

    return { ...result, items };
  }

  async findById(userId: string): Promise<UserDetails> {
    const user = await User.createQueryBuilder('u')
      .innerJoinAndSelect('u.person', 'person')
      .where('u.id = :userId', {
        userId,
      })
      .getOne();

    const userProfiles = await UserProfile.find({
      where: {
        userId,
      },
    });

    const result: UserDetails = {
      active: user.active,
      email: user.email,
      id: user.id,
      name: user.person.name,
      profiles: userProfiles.map((p) => p.profileId),
    };

    return result;
  }

  async findOne(email: string): Promise<User | undefined> {
    return User.findOne({
      where: {
        email,
      },
    });
  }
}
