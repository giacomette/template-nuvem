import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class UserCreateRequest {
  @IsNotEmpty({ message: 'O nome é obrigatório' })
  @ApiProperty({
    required: true,
    example: 'John doe',
    description: 'Nome do usuário',
  })
  readonly name: string;

  @IsNotEmpty({ message: 'O email é obrigatório' })
  @ApiProperty({
    required: true,
    example: 'teste@email.com',
    description: 'Email do cupom',
  })
  readonly email: string;

  @IsNotEmpty({ message: 'O status do usuário é obrigatório' })
  @ApiProperty({
    required: true,
    example: 1,
    enum: [0, 1],
    description: 'status do usuário',
  })
  readonly active: number;

  @IsNotEmpty({ message: 'Informe pelo menos um perfil' })
  @ApiProperty({
    required: true,
    example: ['e9af31dd-43e8-4fa1-97e0-44a200cd276b'],
    description: 'perfis do usuário',
  })
  readonly profiles: string[];
}

export class UserUpdateRequest extends UserCreateRequest {
  @ApiProperty({
    required: false,
    example: 1,
    enum: [0, 1],
    description: 'Informe se é pra resetar a senha do usuário',
  })
  readonly resetPassword: number;
}
