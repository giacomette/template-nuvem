import { BadRequestException, Injectable } from '@nestjs/common';
import { User } from 'infra/database/models/User';
import { DataSource } from 'typeorm';
import { UserCreateRequest, UserUpdateRequest } from './requests';
import { crypt } from 'infra/helpers/bcrypt';
import { UserProfile } from 'infra/database/models/UserProfile';
import { TenantUser } from 'infra/database/models/TenantUser';
import { Person } from 'infra/database/models/Person';
import { PersonTypeEnum } from 'infra/resource/enums/person';

@Injectable()
export class UsersService {
  constructor(private dataSource: DataSource) {}

  async create(data: UserCreateRequest, tenantId: string): Promise<string> {
    const password = '123456';

    const hash = crypt(password);

    const exists = await User.findOneBy({
      email: data.email,
    });

    if (exists) {
      throw new BadRequestException('Usuário já existente');
    }

    return this.dataSource.transaction(async (manager) => {
      const person = new Person({
        name: data.name,
        personTypeId: PersonTypeEnum.Physical,
      });

      await manager.save(person);

      const user = new User({
        active: data.active === 1,
        email: data.email,
        password: hash,
        personId: person.id,
      });

      await manager.save(user);

      const userProfiles = data.profiles.map(
        (item) =>
          new UserProfile({
            userId: user.id,
            profileId: item,
          }),
      );

      await manager.save(userProfiles);

      const tenantUser = new TenantUser({
        userId: user.id,
        tenantId,
      });

      await manager.save(tenantUser);

      return user.id;
    });
  }

  async update(id: string, data: UserUpdateRequest): Promise<string> {
    const user = await User.findOne({
      where: { id },
    });

    const person = await Person.findOneBy({
      id: user.personId,
    });

    if (data.resetPassword === 1) {
      const password = '123456';
      const hash = crypt(password);

      user.password = hash;
    }

    person.name = data.name;

    user.fill({
      active: data.active === 1,
      email: data.email,
    });

    return this.dataSource.transaction(async (manager) => {
      await manager.save(person);
      await manager.save(user);

      await manager.update(
        UserProfile,
        {
          userId: user.id,
        },
        {
          deletedAt: new Date().toISOString(),
        },
      );

      const userProfiles = data.profiles.map(
        (item) =>
          new UserProfile({
            userId: user.id,
            profileId: item,
          }),
      );

      await manager.save(userProfiles);

      return user.id;
    });
  }

  async delete(id: string): Promise<void> {
    const user = await User.findOne({
      where: { id },
    });

    await this.dataSource.transaction(async (manager) => {
      await manager.softRemove(user);
    });
  }
}
