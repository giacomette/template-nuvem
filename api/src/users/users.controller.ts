import {
  Controller,
  Get,
  Post,
  UseGuards,
  Body,
  Put,
  Param,
  Query,
  Delete,
  Req,
} from '@nestjs/common';
import { JwtAuthGuard } from 'auth/guards/jwt-auth.guard';
import { UserCreateRequest, UserUpdateRequest } from './requests';
import { UsersService } from './users.service';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { UsersQueries } from './users.queries';
import { PaginateRequest } from 'infra/helpers/paginate';
import { CheckPermission } from 'infra/guards/permission.decorator';
import { PERMISSIONS } from 'infra/resource/enums/permissions';

@Controller({
  path: 'users',
  version: '1',
})
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiTags('Usuários')
export class UsersController {
  constructor(
    private usersService: UsersService,
    private usersQueries: UsersQueries,
  ) {}

  @Get('/')
  @ApiOperation({
    summary: 'Listar usuários',
  })
  @ApiResponse({
    status: 200,
    schema: {
      example: {
        meta: {
          totalItems: 1,
          itemCount: 1,
          itemsPerPage: 20,
          totalPages: 1,
          currentPage: 1,
        },
        items: [
          {
            active: false,
            email: 'wesley@nuvem.net',
            id: 'e9af31dd-43e8-4fa1-97e0-44a200cd276b',
            name: 'Wesley',
            profiles: ['Admin'],
          },
        ],
      },
    },
  })
  @CheckPermission(PERMISSIONS.Usuario.Visualizar)
  list(
    @Req() req,
    @Query()
    query: PaginateRequest = {
      limit: 20,
      page: 1,
    },
  ) {
    return this.usersQueries.findAll(query, req.headers.tenant);
  }

  @Get(':id')
  @ApiOperation({
    summary: 'Obter usuário',
  })
  @ApiResponse({
    status: 200,
    schema: {
      example: {
        active: false,
        email: 'wesley@nuvem.net',
        id: 'e9af31dd-43e8-4fa1-97e0-44a200cd276b',
        name: 'Wesley',
        profiles: ['e9af31dd-43e8-4fa1-97e0-44a200cd276b'],
      },
    },
  })
  @CheckPermission(PERMISSIONS.Usuario.Visualizar)
  findId(@Param('id') id: string) {
    return this.usersQueries.findById(id);
  }

  @Post('/')
  @ApiOperation({
    summary: 'Criar usuário',
  })
  @ApiResponse({
    status: 200,
    schema: {
      example: {
        data: 'e9af31dd-43e8-4fa1-97e0-44a200cd276b',
      },
    },
    description: `Usuário criado`,
  })
  @CheckPermission(PERMISSIONS.Usuario.Criar)
  public async create(@Req() req, @Body() body: UserCreateRequest) {
    const result = await this.usersService.create(body, req.headers.tenant);

    return { data: result };
  }

  @Put(':id')
  @ApiOperation({
    summary: 'Editar usuário',
  })
  @ApiResponse({
    status: 200,
    schema: {
      example: {
        data: 'e9af31dd-43e8-4fa1-97e0-44a200cd276b',
      },
    },
    description: `Usuário editado`,
  })
  @CheckPermission(PERMISSIONS.Usuario.Editar)
  public async update(
    @Param('id') id: string,
    @Body() body: UserUpdateRequest,
  ) {
    const result = await this.usersService.update(id, body);

    return { data: result };
  }

  @Delete(':id')
  @ApiOperation({
    summary: 'Excluir usuário',
  })
  @ApiResponse({
    status: 200,
    description: `Usuário excluido`,
  })
  @CheckPermission(PERMISSIONS.Usuario.Excluir)
  public async delete(@Param('id') id: string) {
    const result = await this.usersService.delete(id);

    return { data: result };
  }
}
