import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import appConfig from './infra/config';
import { optionsDatabase } from './infra/config/database';
import { ThrottlerModule } from '@nestjs/throttler';

import { AuthModule } from 'auth/auth.module';
import { UsersModule } from 'users/users.module';
import { ProfilesModule } from 'profiles/profiles.module';
import { PermissionsModule } from 'permissions/permissions.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [appConfig],
      isGlobal: true,
    }),
    TypeOrmModule.forRoot(optionsDatabase),
    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 20,
    }),
    AuthModule,
    UsersModule,
    ProfilesModule,
    PermissionsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
