import {
  Container as ContainerChakra,
  ContainerProps as ContainerChakraProps,
} from "@chakra-ui/react";

interface ContainerProps extends ContainerChakraProps {
  children: JSX.Element | JSX.Element[];
}

function Container({ children, ...props }: ContainerProps) {
  return (
    <ContainerChakra maxW="1200px" {...props}>
      {children}
    </ContainerChakra>
  );
}

export default Container;
