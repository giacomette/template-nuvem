import {
  Breadcrumb as BreadcrumbChakra,
  BreadcrumbItem,
  BreadcrumbLink,
} from "@chakra-ui/react";
import { useBreadcrumbStore } from "../../../core/features/breadcrumbs";
import { useNavigate } from "react-router-dom";
import { FaAngleRight } from "react-icons/fa";

function Breadcrumb() {
  const navigate = useNavigate();

  const { items } = useBreadcrumbStore();

  if (!items.length) {
    return null;
  }

  return (
    <BreadcrumbChakra spacing="8px" separator={<FaAngleRight color="white" />}>
      {items.map((item, index) => (
        <BreadcrumbItem key={index} isCurrentPage={items.length - 1 === index}>
          <BreadcrumbLink
            _hover={{ color: "white" }}
            fontSize="13px"
            cursor={items.length - 1 === index ? "text" : "pointer"}
            color={items.length - 1 === index ? "white" : "whiteAlpha.700"}
            onClick={() => {
              if (item.url) {
                navigate(item.url);
              }
            }}
          >
            {item.name}
          </BreadcrumbLink>
        </BreadcrumbItem>
      ))}
    </BreadcrumbChakra>
  );
}

export default Breadcrumb;
