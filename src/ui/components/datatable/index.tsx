import { Box } from "@chakra-ui/react";
import { Table, TableProps } from "antd";

interface DatatableProps<T = any> extends TableProps<T> {}

function Datatable(props: DatatableProps) {
  return (
    <Box>
      <Table
        rowSelection={{
          onChange: () => {},
        }}
        {...props}
      />
    </Box>
  );
}

export default Datatable;
