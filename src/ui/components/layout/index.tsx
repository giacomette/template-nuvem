import { Flex } from "@chakra-ui/react";

import Appbar from "./appbar";
import Header from "./header";

function Layout({ children }: { children: JSX.Element | JSX.Element[] }) {
  return (
    <Flex bg="gray.100" minH="100vh" flexDir="column">
      <Appbar />

      <Header />

      <Flex flexDir={"column"}>{children}</Flex>
    </Flex>
  );
}

export default Layout;
