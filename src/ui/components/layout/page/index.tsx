import { Flex, FlexProps } from "@chakra-ui/react";
import Container from "../../container";

interface PageProps extends FlexProps {
  card?: boolean;
  children: JSX.Element | JSX.Element[];
}

function Page({ children, card, ...props }: PageProps) {
  return (
    <Flex pos="relative" top={-100} flexDir={"column"}>
      <Container
        py="16px"
        rounded={card ? "8px" : undefined}
        shadow={card ? "xs" : undefined}
        bg={card ? "white" : "transparent"}
        {...props}
      >
        {children}
      </Container>
    </Flex>
  );
}

export default Page;
