import { Flex } from "@chakra-ui/react";
import Container from "../../container";
import { primary, primaryLight } from "../../../../infra/resouces/theme/colors";
import Menu from "./components/menu";
import Userinfo from "./components/userinfo";
import Logo from "./components/logo";

function Appbar() {
  return (
    <Flex
      bg={primary}
      py="16px"
      borderBottomColor={primaryLight}
      borderBottomWidth={1}
      zIndex={9}
      w="100%"
      pos={"fixed"}
    >
      <Container>
        <Flex flex={1} gridGap="32px">
          <Flex maxW={150}>
            <Logo />
          </Flex>
          <Flex flex={1}>
            <Menu />
          </Flex>
          <Flex>
            <Userinfo />
          </Flex>
        </Flex>
      </Container>
    </Flex>
  );
}

export default Appbar;
