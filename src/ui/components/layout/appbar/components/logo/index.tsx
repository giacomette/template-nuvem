import { Image } from "@chakra-ui/react";

function Logo() {
  return (
    <Image
      w="100%"
      src={"https://nuvem.net/assets/images/logo-nuvem-tecnologia.svg"}
    />
  );
}

export default Logo;
