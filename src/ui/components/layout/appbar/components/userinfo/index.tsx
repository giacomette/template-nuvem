import { Divider, Flex, Image, Text } from "@chakra-ui/react";
import { useAuthStore } from "../../../../../../core/features/auth/store";
import avatarImg from "../../../../../../assets/images/avatar.png";
import UserinfoMenuItem from "./UserinfoMenuItem";
import Popover from "../../../../popover";
import { useNavigate } from "react-router-dom";
import { useLogout } from "../../../../../../core/features/auth/hooks/useLogout";

function Userinfo() {
  const { user } = useAuthStore();
  const { handleLogout } = useLogout();

  const navigate = useNavigate();

  return (
    <Popover
      trigger={
        <Image
          cursor="pointer"
          border="3px solid rgba(255, 255, 255, .6)"
          w={10}
          h={10}
          rounded="8px"
          src={avatarImg}
        />
      }
      header={
        <Flex gap="8px">
          <Flex>
            <Image w={12} h={12} rounded="8px" src={avatarImg} />
          </Flex>
          <Flex flex={1} alignItems="center">
            <Flex flexDir="column">
              <Text>{user?.name}</Text>
              <Text fontWeight={"normal"} color="gray.500" fontSize="12px">
                {user?.email}
              </Text>
            </Flex>
          </Flex>
        </Flex>
      }
    >
      <Flex flexDir={"column"} gap="2px">
        <UserinfoMenuItem onClick={() => navigate("/profile")}>
          Meu perfil
        </UserinfoMenuItem>
        <UserinfoMenuItem onClick={() => navigate("/settings")}>
          Configuração
        </UserinfoMenuItem>
        <Divider />
        <UserinfoMenuItem onClick={() => handleLogout()}>Sair</UserinfoMenuItem>
      </Flex>
    </Popover>
  );
}

export default Userinfo;
