import { Flex } from "@chakra-ui/react";
import { primaryLight } from "../../../../../../infra/resouces/theme/colors";

function UserinfoMenuItem({
  children,
  onClick,
}: {
  children: any;
  onClick: () => void;
}) {
  return (
    <Flex
      _hover={{
        bg: "#f7f7f7",
        color: primaryLight,
      }}
      cursor="pointer"
      color="gray.600"
      py={2}
      px={2}
      rounded={4}
      onClick={onClick}
    >
      {children}
    </Flex>
  );
}

export default UserinfoMenuItem;
