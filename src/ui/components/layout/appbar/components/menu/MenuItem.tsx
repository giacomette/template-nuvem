import { Flex } from "@chakra-ui/react";

function MenuItem({ children, active }: { children: any; active?: boolean }) {
  return (
    <Flex
      bg={active ? "rgba(255, 255, 255, .1)" : "transparent"}
      _hover={{
        bg: "rgba(255, 255, 255, .1)",
        transition: 'linear .2s all'
      }}
      cursor="pointer"
      px="13px"
      py="9px"
      rounded="4px"
      color="white"
      fontSize="13px"
      fontWeight="semibold"
    >
      {children}
    </Flex>
  );
}

export default MenuItem;
