import { Flex } from "@chakra-ui/react";
import { useMenu } from "../../../../../../core/features/menu/hooks/useMenu";

import MenuItem from "./MenuItem";

function Menu() {
  const { items } = useMenu();

  return (
    <Flex gridGap="4px" alignItems={"center"}>
      {items.map((item, index) => (
        <MenuItem key={index}>{item.title}</MenuItem>
      ))}
    </Flex>
  );
}

export default Menu;
