import { Flex, Text } from "@chakra-ui/react";
import Container from "../../container";
import { primary } from "../../../../infra/resouces/theme/colors";
import Breadcrumb from "../../breadcrumb";

function Header() {
  return (
    <Flex bg={primary} pt="150px" pb="16px" w="100%" h={350}>
      <Container>
        <Flex flex={1} flexDir="column">
          <Text fontWeight="semibold" fontSize="1.4em" color="white">
            Usuários
          </Text>
          <Breadcrumb />
        </Flex>
      </Container>
    </Flex>
  );
}

export default Header;
