import { theme } from "@chakra-ui/react";
import { primary } from "infra/resouces/theme/colors";
import styled from "styled-components";

export const InputContainer = styled.div<any>`
  .chakra-input:hover,
  .chakra-input:focus {
    border-color: ${(props) =>
      props.isInvalid ? theme.colors.red[500] : primary} !important;
    box-shadow: 0 0 0 1px
      ${(props) => (props.isInvalid ? theme.colors.red[500] : primary)} !important;
    outline-color: ${(props) =>
      props.isInvalid ? theme.colors.red[500] : primary};
    transition: all 0.2s;
  }

  .chakra-input {
    border-radius: 6px;
    padding: 9px 16px;
    width: 100%;
    border-width: 1px;
    border-style: solid;
    border-color: ${(props) =>
      props.isInvalid
        ? theme.colors.red[500]
        : theme.colors.blue[100]} !important;
    background-color: ${(props) =>
      props.isInvalid
        ? theme.colors.red[500]
        : theme.colors.blue[100]} !important;
  }
`;
