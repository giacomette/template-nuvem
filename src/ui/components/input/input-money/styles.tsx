import { theme } from "@chakra-ui/react";
import { primary } from "infra/resouces/theme/colors";
import styled from "styled-components";

export const InputContainer = styled.div<any>`
  .chakra-input:hover,
  .chakra-input:focus {
    border-color: ${(props) =>
      props.isInvalid
        ? theme.colors.red[500]
        : !props.disabled
        ? primary
        : "#f2f6f9"} !important;
    box-shadow: 0 0 0 1px
      ${(props) =>
        props.isInvalid
          ? theme.colors.red[500]
          : !props.disabled
          ? primary
          : "#f2f6f9"} !important;
    outline-color: ${(props) =>
      props.isInvalid
        ? theme.colors.red[500]
        : !props.disabled
        ? primary
        : "#f2f6f9"} !important;
    transition: all 0.2s;
  }

  .chakra-input {
    border-radius: 6px;
    padding: 9px 16px;
    width: 100%;
    border: 1px solid
      ${(props) =>
        props.isInvalid
          ? theme.colors.red[500]
          : theme.colors.blue[100]}!important;
    opacity: ${(props) => (props.disabled ? 0.7 : 1)};
    background-color: ${(props) =>
      props.isInvalid
        ? theme.colors.red[500]
        : props.backgroundColor || theme.colors.blue[100]} !important;
  }
`;
