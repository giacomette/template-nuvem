import {
  Popover as PopoverChakra,
  PopoverBody,
  PopoverContent,
  PopoverHeader,
  PopoverTrigger,
} from "@chakra-ui/react";

function Popover({
  children,
  trigger,
  header,
}: {
  children: any;
  trigger: any;
  header: any;
}) {
  return (
    <PopoverChakra placement="top-start">
      <PopoverTrigger>{trigger}</PopoverTrigger>
      <PopoverContent
        py={2}
        boxShadow="rgba(82, 63, 105, .15) 0px 0px 50px 0px"
        borderWidth="0px"
        outlineColor={"transparent"}
      >
        {header ? (
          <PopoverHeader fontWeight="semibold">{header}</PopoverHeader>
        ) : null}

        <PopoverBody>{children}</PopoverBody>
      </PopoverContent>
    </PopoverChakra>
  );
}

export default Popover;
