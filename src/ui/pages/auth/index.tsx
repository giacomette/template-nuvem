import { Route, Routes } from "react-router-dom";
import LoginPage from "./login";

function AuthPages() {
  return (
    <Routes>
      <Route path="/auth/login" element={<LoginPage />} />
    </Routes>
  );
}

export default AuthPages;
