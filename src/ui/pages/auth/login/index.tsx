import { Flex, Image } from "@chakra-ui/react";
import logo from "../../../../assets/images/logo.png";

function LoginPage() {
  return (
    <Flex flex={1} bg="gray.100">
      <Flex alignItems="center">
        <Image w={150} src={logo} />
      </Flex>
    </Flex>
  );
}

export default LoginPage;
