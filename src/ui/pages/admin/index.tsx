import { Outlet, Route, Routes } from "react-router-dom";
import Layout from "../../components/layout";
import HomePage from "./home";
import UsuarioPages from "./usuarios";
import ListagemPage from "./usuarios/listagem";

function AdminPages() {
  return <Route path="a" element={<HomePage />} />;
}

export default AdminPages;
