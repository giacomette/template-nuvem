import { Button, Flex } from "@chakra-ui/react";
import Page from "../../../../components/layout/page";

function CadastroPage() {
  return (
    <Page card>
      <Flex mb="16px" justifyContent={"flex-end"}>
        <Button colorScheme={"blue"}>Salvar</Button>
      </Flex>
    </Page>
  );
}

export default CadastroPage;
