import { Route } from "react-router-dom";
import ListagemPage from "./listagem";
import CadastroPage from "./cadastro";

function UsuarioPages() {
  return (
    <>
      <Route index element={<ListagemPage />} />
      <Route path="new" element={<CadastroPage />} />
      <Route path=":id" element={<CadastroPage />} />
    </>
  );
}

export default UsuarioPages;
