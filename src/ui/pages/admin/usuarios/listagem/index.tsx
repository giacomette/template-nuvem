import { Box, Button, Flex, Image, Tag, Text } from "@chakra-ui/react";
import { AiFillBank } from "react-icons/ai";
import { useNavigate } from "react-router-dom";
import Datatable from "../../../../components/datatable";
import Page from "../../../../components/layout/page";

const dataSource = [
  {
    key: "1",
    nome: "Mike",
    unidade: "Fazenda Itamarati",
    email: "mike@email.com",
    status: "Ativo",
    perfis: ["Admin"],
  },
  {
    key: "2",
    nome: "John",
    unidade: "Fazenda Tucunaré",
    email: "john@email.com",
    status: "Inativo",
    perfis: ["Gestor", "Secretário"],
  },
];

const columns = [
  {
    title: "Nome",
    key: "nome",
    render: (item: any) => (
      <Flex gap="16px">
        <Image
          src="http://localhost:3011/metronic8/react/demo2/media/avatars/300-5.jpg"
          w="40px"
          h="40px"
          rounded="8px"
        />

        <Flex flexDir={"column"}>
          <Text>{item.nome}</Text>
          <Text fontSize="12px" color="gray.600">
            {item.email}
          </Text>
        </Flex>
      </Flex>
    ),
  },
  {
    title: "Unidade",
    dataIndex: "unidade",
    key: "unidade",
    render: (unidade: string) => (
      <Flex gap="4px" alignItems={"center"}>
        <AiFillBank />

        <Text>{unidade}</Text>
      </Flex>
    ),
  },
  {
    title: "Perfis",
    dataIndex: "perfis",
    key: "perfis",
    render: (perfis: string[]) => (
      <Flex gap="16px">
        {perfis.map((perfil, index) => (
          <Tag colorScheme={"blue"} variant="outline" key={index}>
            {perfil}
          </Tag>
        ))}
      </Flex>
    ),
  },

  {
    title: "",
    dataIndex: "status",
    key: "status",
    render: (status: string) => (
      <Flex gap="16px">
        <Tag
          colorScheme={status === "Ativo" ? "green" : "red"}
          variant="subtle"
        >
          {status}
        </Tag>
      </Flex>
    ),
  },
];

function ListagemPage() {
  const navigate = useNavigate();

  return (
    <Page card>
      <Flex mb="16px" justifyContent={"flex-end"}>
        <Button onClick={() => navigate("/users/new")} colorScheme={"blue"}>
          Novo usuário
        </Button>
      </Flex>

      <Box>
        <Datatable dataSource={dataSource} columns={columns} />
      </Box>
    </Page>
  );
}

export default ListagemPage;
