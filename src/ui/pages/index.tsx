import { Outlet, Route, Routes } from "react-router-dom";
import Layout from "../components/layout";
import AdminPages from "./admin";
import HomePage from "./admin/home";
import CadastroPage from "./admin/usuarios/cadastro";
import ListagemPage from "./admin/usuarios/listagem";
import AuthPages from "./auth";
import LoginPage from "./auth/login";

function Pages() {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <Layout>
            <Outlet />
          </Layout>
        }
      >
        <Route path="/" element={<HomePage />} />
        <Route path="users" element={<ListagemPage />} />
        <Route path="users/new" element={<CadastroPage />} />
        <Route path="users/:id" element={<CadastroPage />} />
      </Route>

      <Route path="auth/login" element={<LoginPage />} />
    </Routes>
  );
}

export default Pages;
