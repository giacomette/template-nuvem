import { ChakraProvider } from "@chakra-ui/react";

function ChakraUi({ children }: { children: JSX.Element }) {
  return <ChakraProvider>{children}</ChakraProvider>;
}

export default ChakraUi;
