export interface AuthUser {
  id: string;
  name: string;
  email: string;
  permissions: string[];
}

export interface AuthUserStore {
  user: AuthUser | null;
  update: (user: AuthUser) => void;
  reset: () => void;
}
