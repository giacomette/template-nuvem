import create from "zustand";
import { AuthUser, AuthUserStore } from "../typings";

const defaultValue: AuthUserStore = {
  user: {
    id: "",
    name: "Wesley Teixeira",
    email: "wesley@nuvem.net",
    permissions: [],
  },
  reset() {},
  update(user) {},
};

export const useAuthStore = create<AuthUserStore>((set) => ({
  user: defaultValue.user,
  update: (user: AuthUser) => set(() => ({ user })),
  reset: () => set(() => ({ user: null })),
}));
