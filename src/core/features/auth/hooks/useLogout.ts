import { useNavigate } from "react-router-dom";
import { useAuthStore } from "../store";

export function useLogout() {
  const navigate = useNavigate();
  const { reset } = useAuthStore();

  const handleLogout = () => {
    reset();

    navigate("/auth/login");
  };

  return {
    handleLogout,
  };
}
