import { BASE_URL } from "../../../../infra/config";
import { useQuery } from "react-query";
import { http } from "infra/http";
import { AuthUser } from "../typings";

export function useAuthMe() {
  const url = `${BASE_URL}/auth/me`;

  return useQuery([url], async () => {
    const { data } = await http.get<AuthUser>(url);

    return data;
  });
}
