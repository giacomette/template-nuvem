import { useNavigate } from "react-router-dom";
import { PERMISSIONS } from "../../../../infra/resouces/permissions";
import { MenuItem } from "../typings";

export function useMenu() {
  const navigate = useNavigate();

  const items: MenuItem[] = [
    {
      title: "Inicio",
      onClick() {
        navigate("/");
      },
    },
    {
      title: "Perfis",
      permission: PERMISSIONS.PERFIS.LISTAR,
      onClick() {
        navigate("/profiles");
      },
    },
    {
      title: "Usuários",
      permission: PERMISSIONS.USUARIOS.LISTAR,
      onClick() {
        navigate("/users");
      },
    },
  ];

  return { items };
}
