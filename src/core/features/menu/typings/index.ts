export interface MenuItem {
  title: string;
  permission?: string;
  onClick: () => void;
}
