import ChakraUi from "./infra/packages/@chakra-ui";
import Pages from "./ui/pages";
import { BrowserRouter as Router } from "react-router-dom";

function App() {
  return (
    <Router>
      <ChakraUi>
        <Pages />
      </ChakraUi>
    </Router>
  );
}

export default App;
